
package net.mcreator.energeia.itemgroup;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;

import net.mcreator.energeia.block.PanneauPhotovoltaiqueTier1Block;
import net.mcreator.energeia.EnergeiaModElements;

@EnergeiaModElements.ModElement.Tag
public class EnergeiaItemGroup extends EnergeiaModElements.ModElement {
	public EnergeiaItemGroup(EnergeiaModElements instance) {
		super(instance, 5);
	}

	@Override
	public void initElements() {
		tab = new ItemGroup("tabenergeia") {
			@OnlyIn(Dist.CLIENT)
			@Override
			public ItemStack createIcon() {
				return new ItemStack(PanneauPhotovoltaiqueTier1Block.block);
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return true;
			}
		}.setBackgroundImageName("item_search.png");
	}
	public static ItemGroup tab;
}
